package org.fmavlyutov.command.system;

import org.fmavlyutov.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

public final class SystemHelpCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String getArgument() {
        return "-h";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "display info about commands and arguments";
    }

    @Override
    @NotNull
    public String getName() {
        return "help";
    }

    @Override
    public void execute() {
        for (final AbstractCommand command : getCommandService().getTerminalCommands()) {
            System.out.println(command);
        }
        System.out.println();
    }

}
