package org.fmavlyutov.command.system;

import org.jetbrains.annotations.NotNull;

public final class SystemExitCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "finish program";
    }

    @Override
    @NotNull
    public String getName() {
        return "exit";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
