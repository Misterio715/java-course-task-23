package org.fmavlyutov.command.system;

import org.fmavlyutov.api.service.ICommandService;
import org.fmavlyutov.command.AbstractCommand;
import org.fmavlyutov.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return null;
    }

}
