package org.fmavlyutov.command.system;

import org.jetbrains.annotations.NotNull;

public final class SystemAboutCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String getArgument() {
        return "-a";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "display info about author";
    }

    @Override
    @NotNull
    public String getName() {
        return "about";
    }

    @Override
    public void execute() {
        System.out.println("Author: Philip Mavlyutov\n");
    }
}
