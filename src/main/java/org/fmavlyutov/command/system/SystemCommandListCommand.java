package org.fmavlyutov.command.system;

import org.fmavlyutov.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

public final class SystemCommandListCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String getArgument() {
        return "-cmd";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "display all commands";
    }

    @Override
    @NotNull
    public String getName() {
        return "commands";
    }

    @Override
    public void execute() {
        for (final AbstractCommand command : getCommandService().getTerminalCommands()) {
            if (command == null) {
                continue;
            }
            @NotNull String name = command.getName();
            if (name == null || name.isEmpty()) {
                continue;
            }
            System.out.println(name);
        }
        System.out.println();
    }

}
