package org.fmavlyutov.command.system;

import org.fmavlyutov.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

public final class SystemArgumentListCommand extends AbstractSystemCommand {


    @Override
    @NotNull
    public String getArgument() {
        return "-arg";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "display all commands";
    }

    @Override
    @NotNull
    public String getName() {
        return "arguments";
    }

    @Override
    public void execute() {
        for (final AbstractCommand command : getCommandService().getTerminalCommands()) {
            if (command == null) {
                continue;
            }
            @NotNull String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) {
                continue;
            }
            System.out.println(argument);
        }
        System.out.println();
    }

}
