package org.fmavlyutov.command.system;

import org.jetbrains.annotations.NotNull;

public final class SystemVersionCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String getArgument() {
        return "-v";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "display program version";
    }

    @Override
    @NotNull
    public String getName() {
        return "version";
    }

    @Override
    public void execute() {
        System.out.println("Version: 1.17.0\n");
    }

}
