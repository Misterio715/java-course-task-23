package org.fmavlyutov.command.system;

import org.fmavlyutov.util.NumberUtil;
import org.jetbrains.annotations.NotNull;

public final class SystemInfoCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String getArgument() {
        return "-i";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "display system info";
    }

    @Override
    @NotNull
    public String getName() {
        return "info";
    }

    @Override
    public void execute() {
        long availableProccers = Runtime.getRuntime().availableProcessors();
        System.out.printf("Available processors: %d cores\n", availableProccers);
        long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.printf("Total memory: %s\n", NumberUtil.formatBytes(totalMemory));
        long maxMemory = Runtime.getRuntime().maxMemory();
        String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : NumberUtil.formatBytes(maxMemory);
        System.out.printf("Maximum memory: %s\n", maxMemoryFormat);
        long usedMemory = totalMemory - Runtime.getRuntime().freeMemory();
        System.out.printf("Used memory: %s\n", NumberUtil.formatBytes(usedMemory));
        System.out.println();
    }

}
