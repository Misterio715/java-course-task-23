package org.fmavlyutov.command.project;

import org.jetbrains.annotations.NotNull;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "delete all projects";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-clear";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        getProjectService().clear(getUserId());
    }

}
