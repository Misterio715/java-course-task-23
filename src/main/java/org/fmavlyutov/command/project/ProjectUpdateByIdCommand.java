package org.fmavlyutov.command.project;

import org.fmavlyutov.model.Project;
import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "update project by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-update-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("Enter name:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final Project project = getProjectService().updateById(getUserId(), id, name, description);
        showProject(project);
    }

}
