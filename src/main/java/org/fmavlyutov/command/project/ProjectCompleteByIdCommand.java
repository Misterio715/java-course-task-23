package org.fmavlyutov.command.project;

import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.model.Project;
import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "complete project by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-complete-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT]");
        System.out.println("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Project project = getProjectService().changeStatusById(getUserId(), id, Status.COMPLETED);
        showProject(project);
    }

}
