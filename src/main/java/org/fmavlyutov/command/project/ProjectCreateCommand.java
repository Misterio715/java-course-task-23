package org.fmavlyutov.command.project;

import org.fmavlyutov.model.Project;
import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "create new project";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-create";
    }

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("Enter name:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final Project project = getProjectService().create(getUserId(), name, description);
        showProject(project);
    }

}
