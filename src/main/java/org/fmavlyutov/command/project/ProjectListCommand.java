package org.fmavlyutov.command.project;

import org.fmavlyutov.enumerated.Sort;
import org.fmavlyutov.model.Project;
import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "show list of projects";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-list";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECTS LIST]");
        System.out.println("Enter sort:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sort = TerminalUtil.nextLine();
        @NotNull final List<Project> projects = getProjectService().findAll(getUserId(), Sort.toSort(sort));
        int index = 1;
        for (final Project project : projects) {
            if (project == null) {
                continue;
            }
            System.out.println(index++ + ". " + project.getName());
        }
    }

}
