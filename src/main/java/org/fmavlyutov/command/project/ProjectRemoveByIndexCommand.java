package org.fmavlyutov.command.project;

import org.fmavlyutov.model.Project;
import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "delete project by index";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-remove-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("Enter index:");
        @NotNull final Integer index = TerminalUtil.nextInt() - 1;
        @NotNull final Project project = getProjectService().removeByIndex(getUserId(), index);
        getProjectTaskService().removeProjectById(getUserId(), project.getId());
    }

}
