package org.fmavlyutov.command.project;

import org.fmavlyutov.model.Project;
import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "delete project by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-remove-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Project project = getProjectService().removeById(getUserId(), id);
        getProjectTaskService().removeProjectById(getUserId(), project.getId());
    }

}
