package org.fmavlyutov.command.project;

import org.fmavlyutov.api.service.IProjectService;
import org.fmavlyutov.api.service.IProjectTaskService;
import org.fmavlyutov.command.AbstractCommand;
import org.fmavlyutov.enumerated.Role;
import org.fmavlyutov.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    protected IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    @NotNull
    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    protected void showProject(@NotNull final Project project) {
        System.out.println(project);
    }

}
