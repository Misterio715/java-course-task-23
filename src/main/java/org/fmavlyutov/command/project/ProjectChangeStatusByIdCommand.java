package org.fmavlyutov.command.project;

import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.model.Project;
import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

public final class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "change project status by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-change-status-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String status = TerminalUtil.nextLine();
        @NotNull final Project project = getProjectService().changeStatusById(getUserId(), id, Status.toStatus(status));
        showProject(project);
    }

}
