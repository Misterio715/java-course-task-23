package org.fmavlyutov.command.project;

import org.fmavlyutov.model.Project;
import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "update project by index";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-update-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("Enter index:");
        @NotNull final Integer index = TerminalUtil.nextInt() - 1;
        System.out.println("Enter name:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final Project project = getProjectService().updateByIndex(getUserId(), index, name, description);
        showProject(project);
    }

}
