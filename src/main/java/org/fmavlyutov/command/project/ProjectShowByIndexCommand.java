package org.fmavlyutov.command.project;

import org.fmavlyutov.model.Project;
import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "show project by index";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-show-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("Enter index:");
        @NotNull final Integer index = TerminalUtil.nextInt() - 1;
        @NotNull final Project project = getProjectService().findOneByIndex(getUserId(), index);
        showProject(project);
    }

}
