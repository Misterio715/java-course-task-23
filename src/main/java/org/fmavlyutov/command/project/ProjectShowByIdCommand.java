package org.fmavlyutov.command.project;

import org.fmavlyutov.model.Project;
import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "show project by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-show-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Project project = getProjectService().findOneById(getUserId(), id);
        showProject(project);
    }

}
