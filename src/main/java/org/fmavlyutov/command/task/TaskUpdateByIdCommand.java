package org.fmavlyutov.command.task;

import org.fmavlyutov.model.Task;
import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "update task by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-update-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK]");
        System.out.println("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("Enter name:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final Task task = getTaskService().updateById(getUserId(), id, name, description);
        showTask(task);
    }

}
