package org.fmavlyutov.command.task;

import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.model.Task;
import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

public final class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "change task status by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-change-status-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("Enter index:");
        @NotNull final Integer index = TerminalUtil.nextInt() - 1;
        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String status = TerminalUtil.nextLine();
        @NotNull final Task task = getTaskService().changeStatusByIndex(getUserId(), index, Status.toStatus(status));
        showTask(task);
    }

}
