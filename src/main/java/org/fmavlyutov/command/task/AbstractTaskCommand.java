package org.fmavlyutov.command.task;

import org.fmavlyutov.api.service.IProjectTaskService;
import org.fmavlyutov.api.service.ITaskService;
import org.fmavlyutov.command.AbstractCommand;
import org.fmavlyutov.enumerated.Role;
import org.fmavlyutov.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @NotNull
    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    protected void showTask(@NotNull final Task task) {
        System.out.println(task);
    }

    protected void showTask(@NotNull final Collection<Task> tasks) {
        for (final Task task : tasks) {
            showTask(task);
        }
    }

}
