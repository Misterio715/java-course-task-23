package org.fmavlyutov.command.task;

import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class
TaskRemoveByIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "delete task by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-remove-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        getTaskService().removeById(getUserId(), id);
    }

}
