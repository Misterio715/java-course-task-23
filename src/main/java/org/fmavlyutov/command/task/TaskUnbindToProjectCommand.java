package org.fmavlyutov.command.task;

import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class TaskUnbindToProjectCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "unbind task from project";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-unbind-from-project";
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("Enter project id: ");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter task id: ");
        @NotNull final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().unbindTaskFromProject(getUserId(), projectId, taskId);
    }

}
