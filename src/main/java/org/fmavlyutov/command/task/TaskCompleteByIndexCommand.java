package org.fmavlyutov.command.task;

import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.model.Task;
import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "complete task by index";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-complete-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK]");
        System.out.println("Enter index:");
        @NotNull final Integer index = TerminalUtil.nextInt() - 1;
        @NotNull final Task task = getTaskService().changeStatusByIndex(getUserId(), index, Status.COMPLETED);
        showTask(task);
    }

}
