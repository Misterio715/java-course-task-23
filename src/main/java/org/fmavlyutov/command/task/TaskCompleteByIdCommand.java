package org.fmavlyutov.command.task;

import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.model.Task;
import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "complete task by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-complete-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK]");
        System.out.println("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Task task = getTaskService().changeStatusById(getUserId(), id, Status.COMPLETED);
        showTask(task);
    }

}
