package org.fmavlyutov.command.task;

import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.model.Task;
import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "start task by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-start-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[START TASK]");
        System.out.println("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Task task = getTaskService().changeStatusById(getUserId(), id, Status.IN_PROGRESS);
        showTask(task);
    }

}
