package org.fmavlyutov.command.task;

import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "delete task by index";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-remove-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("Enter index:");
        @NotNull final Integer index = TerminalUtil.nextInt() - 1;
        getTaskService().removeByIndex(getUserId(), index);
    }

}
