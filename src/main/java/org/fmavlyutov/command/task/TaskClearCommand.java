package org.fmavlyutov.command.task;

import org.jetbrains.annotations.NotNull;

public final class TaskClearCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "delete all tasks";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-clear\"";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        getTaskService().clear(getUserId());
    }

}
