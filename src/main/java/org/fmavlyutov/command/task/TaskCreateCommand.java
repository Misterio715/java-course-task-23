package org.fmavlyutov.command.task;

import org.fmavlyutov.model.Task;
import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "create new task";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-create";
    }

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("Enter name:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final Task task = getTaskService().create(getUserId(), name, description);
        showTask(task);
    }

}
