package org.fmavlyutov.command.task;

import org.fmavlyutov.model.Task;
import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "show task by project id";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-show-by-project-id";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY PROJECT ID]");
        System.out.println("Enter project id:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final List<Task> tasks = getTaskService().findAllByProjectId(getUserId(), projectId);
        showTask(tasks);
    }

}
