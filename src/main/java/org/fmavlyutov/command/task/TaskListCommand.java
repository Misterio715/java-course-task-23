package org.fmavlyutov.command.task;

import org.fmavlyutov.enumerated.Sort;
import org.fmavlyutov.model.Task;
import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "show list of tasks";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-list";
    }

    @Override
    public void execute() {
        System.out.println("[TASKS LIST]");
        System.out.println("Enter sort:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sort = TerminalUtil.nextLine();
        @NotNull final List<Task> tasks = getTaskService().findAll(getUserId(), Sort.toSort(sort));
        int index = 1;
        for (Task task : tasks) {
            if (task == null) {
                continue;
            }
            System.out.println(index++ + ". " + task.getName());
        }
    }

}
