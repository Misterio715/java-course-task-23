package org.fmavlyutov.command.user;

import org.fmavlyutov.enumerated.Role;
import org.fmavlyutov.model.User;
import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class UserLockCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @NotNull
    public String getDescription() {
        return "lock user";
    }

    @Override
    @NotNull
    public String getName() {
        return "user-lock";
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("Enter login: ");
        @NotNull final String login = TerminalUtil.nextLine();
        getUserService().lockUserByLogin(login);
    }

}
