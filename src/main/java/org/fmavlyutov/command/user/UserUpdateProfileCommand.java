package org.fmavlyutov.command.user;

import org.fmavlyutov.enumerated.Role;
import org.fmavlyutov.model.User;
import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    @NotNull
    public String getDescription() {
        return "update user profile";
    }

    @Override
    @NotNull
    public String getName() {
        return "user-update-profile";
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[UPDATE USER PROFILE]");
        System.out.println("Enter first name:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter last name:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter middle name:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        @NotNull final User user = getUserService().updateUser(userId, firstName, lastName, middleName);
        showUser(user);
    }

}
