package org.fmavlyutov.command.user;

import org.fmavlyutov.enumerated.Role;
import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    @NotNull
    public String getDescription() {
        return "change user password";
    }

    @Override
    @NotNull
    public String getName() {
        return "user-change-password";
    }

    @Override
    public void execute() {
        @NotNull final String userId = getAuthService().getUserId();
        System.out.println("[CHANGE USER PASSWORD]");
        System.out.println("Enter new password");
        @NotNull final String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }

}
