package org.fmavlyutov.command.user;

import org.fmavlyutov.enumerated.Role;
import org.jetbrains.annotations.NotNull;

public final class UserLogoutCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    @NotNull
    public String getDescription() {
        return "logout user";
    }

    @Override
    @NotNull
    public String getName() {
        return "logout";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT USER]");
        getAuthService().logout();
    }

}
