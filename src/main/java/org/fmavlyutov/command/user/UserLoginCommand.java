package org.fmavlyutov.command.user;

import org.fmavlyutov.enumerated.Role;
import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class UserLoginCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public Role[] getRoles() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "login user";
    }

    @Override
    @NotNull
    public String getName() {
        return "login";
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN USER]");
        System.out.println("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("Enter password");
        @NotNull final String password = TerminalUtil.nextLine();
        getAuthService().login(login, password);
    }

}
