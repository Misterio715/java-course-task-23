package org.fmavlyutov.command.user;

import org.fmavlyutov.api.service.IAuthService;
import org.fmavlyutov.api.service.IUserService;
import org.fmavlyutov.command.AbstractCommand;
import org.fmavlyutov.enumerated.Role;
import org.fmavlyutov.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected IAuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

    @NotNull
    protected IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    protected void showUser(@NotNull final User user) {
        System.out.println(user);
    }

}
