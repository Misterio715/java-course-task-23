package org.fmavlyutov.command.user;

import org.fmavlyutov.enumerated.Role;
import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class UserUnlockCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @NotNull
    public String getDescription() {
        return "unlock user";
    }

    @Override
    @NotNull
    public String getName() {
        return "user-unlock";
    }

    @Override
    public void execute() {
        System.out.println("[UNLOCK USER]");
        System.out.println("Enter login: ");
        @NotNull final String login = TerminalUtil.nextLine();
        getUserService().unlockUserByLogin(login);
    }

}
