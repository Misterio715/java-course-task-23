package org.fmavlyutov.command.user;

import org.fmavlyutov.enumerated.Role;
import org.fmavlyutov.model.User;
import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class UserRegistryCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    @NotNull
    public String getDescription() {
        return "registry user";
    }

    @Override
    @NotNull
    public String getName() {
        return "user-registry";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY USER]");
        System.out.println("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("Enter email:");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final User user = getAuthService().registry(login, password, email);
        showUser(user);
    }

}
