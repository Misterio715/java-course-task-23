package org.fmavlyutov.command.user;

import org.fmavlyutov.enumerated.Role;
import org.fmavlyutov.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class UserRemoveByEmailCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @NotNull
    public String getDescription() {
        return "remove user by email";
    }

    @Override
    @NotNull
    public String getName() {
        return "user-remove-by-email";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER BY EMAIL]");
        System.out.println("Enter email: ");
        @NotNull final String email = TerminalUtil.nextLine();
        getUserService().removeByEmail(email);
    }

}
