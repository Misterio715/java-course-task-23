package org.fmavlyutov.command.user;

import org.fmavlyutov.enumerated.Role;
import org.fmavlyutov.model.User;
import org.jetbrains.annotations.NotNull;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    @NotNull
    public String getDescription() {
        return "view user profile";
    }

    @Override
    @NotNull
    public String getName() {
        return "user-view-profile";
    }

    @Override
    public void execute() {
        @NotNull final User user = getAuthService().getUser();
        System.out.println("[VIEW USER PROFILE]");
        showUser(user);
    }

}
