package org.fmavlyutov.command;

import lombok.Getter;
import lombok.Setter;
import org.fmavlyutov.api.model.ICommand;
import org.fmavlyutov.api.service.IServiceLocator;
import org.fmavlyutov.component.Bootstrap;
import org.fmavlyutov.enumerated.Role;
import org.fmavlyutov.exception.system.EmptyServiceLocatorException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractCommand implements ICommand {

    @Nullable
    protected IServiceLocator serviceLocator;

    @NotNull
    public abstract Role[] getRoles();

    @NotNull
    public IServiceLocator getServiceLocator() {
        if (serviceLocator == null) {
            throw new EmptyServiceLocatorException();
        }
        return serviceLocator;
    }

    public void setServiceLocator(@Nullable final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public String getUserId() {
        return getServiceLocator().getAuthService().getUserId();
    }

    @Override
    @NotNull
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "[";
        if (name != null && !name.isEmpty()) {
            result += name;
        }
        if (argument != null && !argument.isEmpty()) {
            result += " | " + argument;
        }
        result += "]";
        if (description != null && !description.isEmpty()) {
            result += " - " + description;
        }
        return result;
    }

}
