package org.fmavlyutov.repository;

import org.fmavlyutov.api.repository.IRepository;
import org.fmavlyutov.enumerated.Sort;
import org.fmavlyutov.model.AbstractModel;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final List<M> records = new ArrayList<>();

    @Override
    @NotNull
    public M add(@NotNull final M model) {
        records.add(model);
        return model;
    }

    @Override
    @NotNull
    public List<M> findAll() {
        return records;
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        @NotNull final List<M> result = new ArrayList<>(records);
        result.sort(comparator);
        return result;
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final Sort sort) {
        return findAll(sort.getComparator());
    }

    @Override
    @Nullable
    public M findOneById(@NotNull final String id) {
        return records.stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public M findOneByIndex(@NotNull final Integer index) {
        return records.get(index);
    }

    @Override
    @NotNull
    public M remove(@NotNull final M model) {
        records.remove(model);
        return model;
    }

    @Override
    @Nullable
    public M removeById(@NotNull final String id) {
        @Nullable final M model = findOneById(id);
        if (model == null) {
            return null;
        }
        return remove(model);
    }

    @Override
    @NotNull
    public M removeByIndex(@NotNull final Integer index) {
        @Nullable final M model = findOneByIndex(index);
        if (model == null) {
            return null;
        }
        return remove(model);
    }

    public void removeAll(@NotNull final List<M> models) {
        records.removeAll(models);
    }

    @Override
    public void clear() {
        records.clear();
    }

    @Override
    @NotNull
    public Integer getSize() {
        return records.size();
    }

    @Override
    @NotNull
    public Boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

}
