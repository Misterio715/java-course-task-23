package org.fmavlyutov.repository;

import org.fmavlyutov.api.repository.ICommandRepository;
import org.fmavlyutov.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> mapByArgument = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> mapByName = new LinkedHashMap<>();

    @Override
    public void add(@Nullable final AbstractCommand command) {
        if (command == null) {
            return;
        }
        @NotNull final String name = command.getName();
        if (name != null && !name.isEmpty()) {
            mapByName.put(name, command);
        }
        @Nullable final String argument = command.getArgument();
        if (argument != null && !argument.isEmpty()) {
            mapByArgument.put(argument, command);
        }
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByArgument(@Nullable final String argument) {
        return mapByArgument.getOrDefault(argument, null);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByName(@Nullable final String name) {
        return mapByName.getOrDefault(name, null);
    }

    @Override
    @NotNull
    public Collection<AbstractCommand> getTerminalCommands() {
        return mapByName.values();
    }

}
