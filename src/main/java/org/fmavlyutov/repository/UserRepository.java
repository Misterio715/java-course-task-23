package org.fmavlyutov.repository;

import org.fmavlyutov.api.repository.IUserRepository;
import org.fmavlyutov.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    @Nullable
    public User findOneByLogin(@NotNull final String login) {
        return records.stream()
                .filter(m -> login.equals(m.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public User findOneByEmail(@NotNull final String email) {
        return records.stream()
                .filter(m -> email.equals(m.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @NotNull
    public Boolean isLoginExists(@NotNull final String login) {
        return records.stream()
                .anyMatch(m -> login.equals(m.getLogin()));
    }

    @Override
    @NotNull
    public Boolean isEmailExists(@NotNull final String email) {
        return records.stream()
                .anyMatch(m -> email.equals(m.getEmail()));
    }

}
