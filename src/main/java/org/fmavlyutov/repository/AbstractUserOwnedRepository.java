package org.fmavlyutov.repository;

import org.fmavlyutov.api.repository.IUserOwnedRepository;
import org.fmavlyutov.enumerated.Sort;
import org.fmavlyutov.model.AbstractUserOwnedModel;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    @Nullable
    public M add(@Nullable final String userId, @NotNull M model) {
        if (userId == null) {
            return null;
        }
        model.setUserId(userId);
        return add(model);
    }

    @Override
    @NotNull
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null) {
            return Collections.emptyList();
        }
        return records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    @NotNull
    public List<M> findAll(@Nullable final String userId, @NotNull final Comparator<M> comparator) {
        @NotNull final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    @NotNull
    public List<M> findAll(@Nullable final String userId, @NotNull final Sort sort) {
        @NotNull final List<M> result = findAll(userId);
        result.sort(sort.getComparator());
        return result;
    }

    @Override
    @Nullable
    public M findOneById(@Nullable final String userId, @NotNull final String id) {
        if (userId == null) {
            return null;
        }
        return records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public M findOneByIndex(@Nullable final String userId, @NotNull final Integer index) {
        return records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public M remove(@Nullable final String userId, @NotNull final M model) {
        if (userId == null) {
            return null;
        }
        return removeById(userId, model.getId());
    }

    @Override
    @Nullable
    public M removeById(@Nullable final String userId, @NotNull final String id) {
        if (userId == null) {
            return null;
        }
        @Nullable final M model = findOneById(userId, id);
        if (model == null) {
            return null;
        }
        return remove(model);
    }

    @Override
    @Nullable
    public M removeByIndex(@Nullable final String userId, @NotNull final Integer index) {
        if (userId == null) {
            return null;
        }
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) {
            return null;
        }
        return remove(model);
    }

    @Override
    public void clear(@Nullable final String userId) {
        @NotNull final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    @NotNull
    public Integer getSize(@Nullable final String userId) {
        return (int) records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .count();
    }

    @Override
    public Boolean existsById(@Nullable final String userId, final String id) {
        return findOneById(userId, id) != null;
    }

}
