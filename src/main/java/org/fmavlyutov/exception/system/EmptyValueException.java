package org.fmavlyutov.exception.system;

public final class EmptyValueException extends AbstractSystemException {

    public EmptyValueException() {
        super("Value can not be empty!");
    }

}
