package org.fmavlyutov.exception.system;

public final class EmptyServiceLocatorException extends AbstractSystemException {

    public EmptyServiceLocatorException() {
        super("Service locator must be defined!");
    }

}
