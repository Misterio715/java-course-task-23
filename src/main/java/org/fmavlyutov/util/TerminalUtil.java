package org.fmavlyutov.util;

import org.fmavlyutov.exception.system.ParseNumberException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Scanner;

import static java.lang.System.in;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextInt() {
        try {
            return Integer.parseInt(SCANNER.nextLine());
        } catch (NumberFormatException e) {
            throw new ParseNumberException(e);
        }
    }

}
