package org.fmavlyutov.util;

import org.fmavlyutov.exception.system.EmptyValueException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    @NotNull
    String SECRET = "34534";

    @NotNull
    Integer ITERATION = 7657;

    @NotNull
    static String salt(@Nullable final String value) {
        if (value == null) {
            throw new EmptyValueException();
        }
        @Nullable String result = value;
        for (int i = 0; i < ITERATION; i++) {
            result = md5(SECRET + value + SECRET);
        }
        return result;
    }

    @NotNull
    static String md5(@Nullable final String value) {
        if (value == null) {
            throw new EmptyValueException();
        }
        try {
            @NotNull final MessageDigest md = MessageDigest.getInstance("md5");
            @NotNull final byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; i++) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (@NotNull NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}
