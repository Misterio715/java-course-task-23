package org.fmavlyutov.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.fmavlyutov.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractModel {

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @NotNull
    private Role role = Role.USUAL;

    @NotNull
    private Boolean locked = false;

    @Override
    @NotNull
    public String toString() {
        StringBuffer sb = new StringBuffer(
                "id: " + id + "\n" +
                        "login: " + login + "\n" +
                        "email: " + email + "\n" +
                        "role: " + role.getDisplayName());
        if (firstName != null) {
            sb.append("firstName: " + firstName + "\n");
        }
        if (lastName != null) {
            sb.append("lastName: " + lastName + "\n");
        }
        if (middleName != null) {
            sb.append("middleName: " + middleName + "\n");
        }
        return sb.toString();
    }

    @NotNull
    public Boolean isLocked() {
        return locked;
    }

}
