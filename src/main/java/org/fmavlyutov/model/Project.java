package org.fmavlyutov.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.fmavlyutov.api.model.IWBS;
import org.fmavlyutov.enumerated.Status;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    private Date created = new Date();

    public Project(@Nullable String name, @Nullable String description, @Nullable Status status) {
        this.name = name;
        this.description = description;
        this.status = status;
    }

    @Override
    @NotNull
    public String toString() {
        return "id: " + id + "\n" +
                "name: " + name + "\n" +
                "description: " + description + "\n" +
                "status: " + Status.toName(status) + "\n" +
                "created: " + created;
    }

}
