package org.fmavlyutov.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.fmavlyutov.api.model.IWBS;
import org.fmavlyutov.enumerated.Status;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    private String projectId;

    @NotNull
    private Date created = new Date();

    public Task(@Nullable String name, @Nullable String description) {
        this.name = name;
        this.description = description;
    }

    public Task(@Nullable String id, @Nullable String name, @Nullable String description, @Nullable Status status, @Nullable String projectId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.status = status;
        this.projectId = projectId;
    }

    @Override
    @NotNull
    public String toString() {
        return "id: " + id + "\n" +
                "name: " + name + "\n" +
                "description: " + description +  "\n" +
                "status: " + Status.toName(status) + "\n" +
                "project id: " + projectId + "\n" +
                "created: " + created;
    }

}
