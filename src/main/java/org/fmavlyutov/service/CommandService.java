package org.fmavlyutov.service;

import org.fmavlyutov.api.repository.ICommandRepository;
import org.fmavlyutov.api.service.ICommandService;
import org.fmavlyutov.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public void add(@Nullable final AbstractCommand command) {
        if (command == null) {
            return;
        }
        commandRepository.add(command);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByArgument(@Nullable final String argument) {
        if (argument == null || argument.isEmpty()) {
            return null;
        }
        return commandRepository.getCommandByArgument(argument);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByName(@Nullable String name) {
        if (name == null || name.isEmpty()) {
            return null;
        }
        return commandRepository.getCommandByName(name);
    }

    @Override
    @NotNull
    public Collection<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
