package org.fmavlyutov.service;

import org.fmavlyutov.api.repository.IUserOwnedRepository;
import org.fmavlyutov.api.service.IUserOwnedService;
import org.fmavlyutov.enumerated.Sort;
import org.fmavlyutov.exception.entity.EmptyModelException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIdException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIndexException;
import org.fmavlyutov.exception.user.EmptyUserIdException;
import org.fmavlyutov.model.AbstractUserOwnedModel;
import org.fmavlyutov.repository.AbstractUserOwnedRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>> extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull R repository) {
        super(repository);
    }

    @Override
    @NotNull
    public M add(@Nullable final String userId, @Nullable M model) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (model == null) {
            throw new EmptyModelException();
        }
        return repository.add(userId, model);
    }

    @Override
    @NotNull
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        return repository.findAll(userId);
    }

    @Override
    @NotNull
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator<M> comparator) {
        if (comparator == null) {
            return findAll(userId);
        }
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        return repository.findAll(userId, comparator);
    }

    @Override
    @NotNull
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (sort == null) {
            return findAll(userId);
        }
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        return repository.findAll(userId, sort.getComparator());
    }

    @Override
    @NotNull
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        final M model = repository.findOneById(userId, id);
        if (model == null) {
            throw new EmptyModelException();
        }
        return model;
    }

    @Override
    @NotNull
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (index == null || index < 0 || index >= repository.getSize()) {
            throw new InvalidOrEmptyIndexException();
        }
        final M model = repository.findOneByIndex(userId, index);
        if (model == null) {
            throw new EmptyModelException();
        }
        return model;
    }

    @Override
    @NotNull
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (model == null) {
            throw new EmptyModelException();
        }
        return repository.remove(userId, model);
    }

    @Override
    @NotNull
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        return repository.removeById(userId, id);
    }

    @Override
    @NotNull
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (index == null || index < 0 || index >= repository.getSize()) {
            throw new InvalidOrEmptyIndexException();
        }
        return repository.removeByIndex(userId, index);
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        repository.clear(userId);
    }

    @Override
    @NotNull
    public Integer getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        return repository.getSize(userId);
    }

    @Override
    @NotNull
    public Boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        return repository.existsById(userId, id);
    }

}
