package org.fmavlyutov.service;

import lombok.Getter;
import org.fmavlyutov.api.service.ILoggerService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.logging.*;

public final class LoggerService implements ILoggerService {

    @NotNull
    private static final String CONFIG_FILE = "/logger.properties";

    @NotNull
    private static final String COMMANDS = "COMMANDS";

    @NotNull
    private static final String COMMANDS_FILE = "./commands.xml";

    @NotNull
    private static final String ERRORS = "ERRORS";

    @NotNull
    private static final String ERRORS_FILE = "./errors.xml";

    @NotNull
    private static final String MESSAGES = "MESSAGES";

    @NotNull
    private static final String MESSAGES_FILE = "./messages.xml";

    @NotNull
    private static final LogManager MANAGER = LogManager.getLogManager();

    @NotNull
    private static final Logger LOGGER_ROOT = Logger.getLogger("");

    @NotNull
    @Getter
    private static final Logger LOGGER_COMMAND = Logger.getLogger(COMMANDS);

    @NotNull
    @Getter
    private static final Logger LOGGER_ERROR = Logger.getLogger(ERRORS);

    @NotNull
    @Getter
    private static final Logger LOGGER_MESSAGE = Logger.getLogger(MESSAGES);

    @NotNull
    private static final ConsoleHandler CONSOLE_HANDLER = getConsoleHandler();

    @NotNull
    private static ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    static {
        loadConfigFromFile();
        registry(LOGGER_COMMAND, COMMANDS_FILE, false);
        registry(LOGGER_ERROR, ERRORS_FILE, true);
        registry(LOGGER_MESSAGE, MESSAGES_FILE, true);
    }

    private static void loadConfigFromFile() {
        try {
            MANAGER.readConfiguration(LoggerService.class.getResourceAsStream(CONFIG_FILE));
        } catch (IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    private static void registry(@NotNull final Logger logger, @NotNull final String fileName, final boolean isConsole) {
        try {
            if (isConsole) {
                logger.addHandler(CONSOLE_HANDLER);
            }
            logger.setUseParentHandlers(false);
            if (fileName != null || !fileName.isEmpty()) {
                logger.addHandler(new FileHandler(fileName));
            }
        } catch (IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    @Override
    public void info(@Nullable final String message) {
        if (message == null || message.isEmpty()) {
            return;
        }
        LOGGER_MESSAGE.info(message);
    }

    @Override
    public void command(@Nullable final String message) {
        if (message == null || message.isEmpty()) {
            return;
        }
        LOGGER_COMMAND.info(message);
    }

    @Override
    public void debug(@Nullable final String message) {
        if (message == null || message.isEmpty()) {
            return;
        }
        LOGGER_MESSAGE.fine(message);
    }

    @Override
    public void error(@Nullable final Exception e) {
        if (e == null) {
            return;
        }
        LOGGER_ERROR.log(Level.SEVERE, e.getMessage(), e);
    }

}
