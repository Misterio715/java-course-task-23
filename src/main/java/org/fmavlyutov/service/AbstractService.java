package org.fmavlyutov.service;

import org.fmavlyutov.api.repository.IRepository;
import org.fmavlyutov.api.service.IService;
import org.fmavlyutov.enumerated.Sort;
import org.fmavlyutov.exception.entity.EmptyModelException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIdException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIndexException;
import org.fmavlyutov.model.AbstractModel;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected R repository;

    public AbstractService(@NotNull R repository) {
        this.repository = repository;
    }

    @Override
    @NotNull
    public M add(@Nullable final M model) {
        if (model == null) {
            throw new EmptyModelException();
        }
        return repository.add(model);
    }

    @Override
    @NotNull
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    @NotNull
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) {
            return findAll();
        }
        return repository.findAll(comparator);
    }

    @Override
    @NotNull
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) {
            return findAll();
        }
        return repository.findAll(sort.getComparator());
    }

    @Override
    @NotNull
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        @Nullable final M model = repository.findOneById(id);
        if (model == null) {
            throw new EmptyModelException();
        }
        return model;
    }

    @Override
    @NotNull
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0 || index >= repository.getSize()) {
            throw new InvalidOrEmptyIndexException();
        }
        @Nullable final M model = repository.findOneByIndex(index);
        if (model == null) {
            throw new EmptyModelException();
        }
        return model;
    }

    @Override
    @NotNull
    public M remove(@Nullable final M model) {
        if (model == null) {
            throw new EmptyModelException();
        }
        return repository.remove(model);
    }

    @Override
    @NotNull
    public M removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        @Nullable final M model = repository.removeById(id);
        if (model == null) {
            throw new EmptyModelException();
        }
        return model;
    }

    @Override
    @NotNull
    public M removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0 || index >= repository.getSize()) {
            throw new InvalidOrEmptyIndexException();
        }
        @Nullable final M model = repository.removeByIndex(index);
        if (model == null) {
            throw new EmptyModelException();
        }
        return model;
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    @NotNull
    public Integer getSize() {
        return repository.getSize();
    }

    @Override
    @NotNull
    public Boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        return repository.existsById(id);
    }

}
