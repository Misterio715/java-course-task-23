package org.fmavlyutov.service;

import org.fmavlyutov.api.repository.IProjectRepository;
import org.fmavlyutov.api.service.IProjectService;
import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.exception.entity.ProjectNotFoundException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIdException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIndexException;
import org.fmavlyutov.exception.field.InvalidOrEmptyNameException;
import org.fmavlyutov.exception.user.EmptyUserIdException;
import org.fmavlyutov.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull IProjectRepository repository) {
        super(repository);
    }

    @Override
    @NotNull
    public Project create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (name == null || name.isEmpty()) {
            throw new InvalidOrEmptyNameException();
        }
        @NotNull final Project project = new Project();
        project.setName(name);
        if (description != null) {
            project.setDescription(description);
        }
        return add(userId, project);
    }

    @Override
    @NotNull
    public Project updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        if (name == null || name.isEmpty()) {
            throw new InvalidOrEmptyNameException();
        }
        @Nullable final Project project = repository.findOneById(userId, id);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    @NotNull
    public Project updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (index == null || index < 0 || index >= repository.getSize()) {
            throw new InvalidOrEmptyIndexException();
        }
        if (name == null || name.isEmpty()) {
            throw new InvalidOrEmptyNameException();
        }
        @Nullable final Project project = repository.findOneByIndex(userId, index);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    @NotNull
    public Project changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        @Nullable final Project project = repository.findOneById(userId, id);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        project.setStatus(status);
        return project;
    }

    @Override
    @NotNull
    public Project changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (index == null || index < 0 || index >= repository.getSize()) {
            throw new InvalidOrEmptyIndexException();
        }
        @Nullable final Project project = repository.findOneByIndex(userId, index);
        if (project == null) {
            throw new ProjectNotFoundException();
        }
        project.setStatus(status);
        return project;
    }

}
