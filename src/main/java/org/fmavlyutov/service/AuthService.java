package org.fmavlyutov.service;

import org.fmavlyutov.api.service.IAuthService;
import org.fmavlyutov.api.service.IUserService;
import org.fmavlyutov.enumerated.Role;
import org.fmavlyutov.exception.field.InvalidOrEmptyLoginException;
import org.fmavlyutov.exception.field.InvalidOrEmptyPasswordException;
import org.fmavlyutov.exception.user.AccessDeniedException;
import org.fmavlyutov.exception.user.PermissionException;
import org.fmavlyutov.model.User;
import org.fmavlyutov.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull IUserService userService) {
        this.userService = userService;
    }

    @Override
    @NotNull
    public User registry(@Nullable String login, @Nullable String password, @Nullable String email) {
        return userService.create(login, password, email);
    }

    @Override
    public void login(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) {
            throw new InvalidOrEmptyLoginException();
        }
        if (password == null | password.isEmpty()) {
            throw new InvalidOrEmptyPasswordException();
        }
        @NotNull final User user = userService.findOneByLogin(login);
        if (user == null) {
            throw new AccessDeniedException();
        }
        final boolean locked = user.isLocked() == null || user.isLocked();
        if (locked) {
            throw new AccessDeniedException();
        }
        @NotNull final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) {
            throw new AccessDeniedException();
        }
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    @NotNull
    public String getUserId() {
        if (!isAuth()) {
            throw new AccessDeniedException();
        }
        return userId;
    }

    @Override
    @NotNull
    public User getUser() {
        if (!isAuth()) {
            throw new AccessDeniedException();
        }
        @NotNull final User user = userService.findOneById(userId);
        if (user == null) {
            throw new AccessDeniedException();
        }
        return user;
    }

    @Override
    public void checkRoles(@Nullable Role[] roles) {
        if (roles == null) {
            return;
        }
        @NotNull final User user = getUser();
        @NotNull final Role role = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) {
            throw new PermissionException();
        }
    }

}
