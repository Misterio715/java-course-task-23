package org.fmavlyutov.service;

import org.fmavlyutov.api.repository.ITaskRepository;
import org.fmavlyutov.api.service.ITaskService;
import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.exception.entity.TaskNotFoundException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIdException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIndexException;
import org.fmavlyutov.exception.field.InvalidOrEmptyNameException;
import org.fmavlyutov.exception.user.EmptyUserIdException;
import org.fmavlyutov.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.List;

public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull ITaskRepository repository) {
        super(repository);
    }

    @Override
    @NotNull
    public Task create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (name == null || name.isEmpty()) {
            throw new InvalidOrEmptyNameException();
        }
        @NotNull final Task task = new Task();
        task.setName(name);
        if (description != null) {
            task.setDescription(description);
        }
        return add(userId, task);
    }

    @Override
    @NotNull
    public Task updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        if (name == null | name.isEmpty()) {
            throw new InvalidOrEmptyNameException();
        }
        @Nullable final Task task = repository.findOneById(userId, id);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    @NotNull
    public Task updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (index == null || index < 0 || index >= repository.getSize()) {
            throw new InvalidOrEmptyIndexException();
        }
        if (name == null | name.isEmpty()) {
            throw new InvalidOrEmptyNameException();
        }
        @Nullable final Task task = repository.findOneByIndex(userId, index);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    @NotNull
    public Task changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        @Nullable final Task task = repository.findOneById(userId, id);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        task.setStatus(status);
        return task;
    }

    @Override
    @NotNull
    public Task changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (index == null || index < 0 || index >= repository.getSize()) {
            throw new InvalidOrEmptyIndexException();
        }
        @Nullable final Task task = repository.findOneByIndex(userId, index);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        task.setStatus(status);
        return task;
    }

    @Override
    @NotNull
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (id == null || id.isEmpty()) {
            return Collections.emptyList();
        }
        return repository.findAllByProjectId(userId, id);
    }

}
