package org.fmavlyutov.service;

import org.fmavlyutov.api.repository.IProjectRepository;
import org.fmavlyutov.api.repository.ITaskRepository;
import org.fmavlyutov.api.repository.IUserRepository;
import org.fmavlyutov.api.service.IUserService;
import org.fmavlyutov.enumerated.Role;
import org.fmavlyutov.exception.entity.UserNotFoundException;
import org.fmavlyutov.exception.field.InvalidOrEmptyEmailException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIdException;
import org.fmavlyutov.exception.field.InvalidOrEmptyLoginException;
import org.fmavlyutov.exception.field.InvalidOrEmptyPasswordException;
import org.fmavlyutov.exception.user.EmptyRoleException;
import org.fmavlyutov.exception.user.ExistsEmailException;
import org.fmavlyutov.exception.user.ExistsLoginException;
import org.fmavlyutov.model.User;
import org.fmavlyutov.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    public UserService(@NotNull IUserRepository repository, @NotNull IProjectRepository projectRepository, @NotNull ITaskRepository taskRepository) {
        super(repository);
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    @NotNull
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) {
            throw new InvalidOrEmptyLoginException();
        }
        if (isLoginExists(login)) {
            throw new ExistsLoginException();
        }
        if (password == null || password.isEmpty()) {
            throw new InvalidOrEmptyPasswordException();
        }
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return repository.add(user);
    }

    @Override
    @NotNull
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) {
            throw new InvalidOrEmptyLoginException();
        }
        if (isLoginExists(login)) {
            throw new ExistsLoginException();
        }
        if (password == null || password.isEmpty()) {
            throw new InvalidOrEmptyPasswordException();
        }
        if (isEmailExists(email)) {
            throw new ExistsEmailException();
        }
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    @NotNull
    public User create(@Nullable final String login, @Nullable final String password, @NotNull final Role role) {
        if (login == null || login.isEmpty()) {
            throw new InvalidOrEmptyLoginException();
        }
        if (isLoginExists(login)) {
            throw new ExistsLoginException();
        }
        @NotNull final User user = create(login, password);
        if (role != null) {
            user.setRole(role);
        }
        return user;
    }

    @Override
    @NotNull
    public User findOneByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) {
            throw new InvalidOrEmptyLoginException();
        }
        @Nullable final User user = repository.findOneByLogin(login);
        if (user == null) {
            throw new UserNotFoundException();
        }
        return user;
    }

    @Override
    @NotNull
    public User findOneByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) {
            throw new InvalidOrEmptyEmailException();
        }
        @Nullable final User user = repository.findOneByEmail(email);
        if (user == null) {
            throw new UserNotFoundException();
        }
        return user;
    }

    @Override
    @NotNull
    public User remove(@Nullable final User model) {
        if (model == null) {
            return null;
        }
        @NotNull final User user = super.remove(model);
        @NotNull final String userId = user.getId();
        taskRepository.clear(userId);
        projectRepository.clear(userId);
        return user;
    }

    @Override
    @NotNull
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) {
            throw new InvalidOrEmptyLoginException();
        }
        @Nullable final User user = repository.findOneByLogin(login);
        if (user == null) {
            throw new UserNotFoundException();
        }
        return remove(user);
    }

    @Override
    @NotNull
    public User removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) {
            throw new InvalidOrEmptyEmailException();
        }
        @Nullable final User user = repository.findOneByEmail(email);
        if (user == null) {
            throw new UserNotFoundException();
        }
        return remove(user);
    }

    @Override
    @NotNull
    public User setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        if (password == null || password.isEmpty()) {
            throw new InvalidOrEmptyPasswordException();
        }
        @Nullable final User user = repository.findOneById(id);
        if (user == null) {
            throw new UserNotFoundException();
        }
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    @NotNull
    public User updateUser(@Nullable final String id, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName) {
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        @Nullable final User user = repository.findOneById(id);
        if (user == null) {
            throw new UserNotFoundException();
        }
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) {
            return false;
        }
        return repository.isLoginExists(login);
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) {
            return false;
        }
        return repository.isEmailExists(email);
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) {
            throw new InvalidOrEmptyLoginException();
        }
        @Nullable final User user = repository.findOneByLogin(login);
        if (user == null) {
            throw new UserNotFoundException();
        }
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) {
            throw new InvalidOrEmptyLoginException();
        }
        @Nullable final User user = repository.findOneByLogin(login);
        if (user == null) {
            throw new UserNotFoundException();
        }
        user.setLocked(false);
    }

}
