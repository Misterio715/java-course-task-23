package org.fmavlyutov.enumerated;

import lombok.Getter;
import org.fmavlyutov.comparator.CreatedComparator;
import org.fmavlyutov.comparator.NameComparator;
import org.fmavlyutov.comparator.StatusComparator;
import org.fmavlyutov.model.AbstractModel;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;

public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE);

    @NotNull
    @Getter
    private final String displayName;

    @NotNull
    private final Comparator<?> comparator;

    Sort(@NotNull String displayName, @NotNull Comparator<?> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public static Sort toSort(final String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }
        for (final Sort sort : values()) {
            if (sort.name().equals(value)) {
                return sort;
            }
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public <T> Comparator<T> getComparator() {
        return (Comparator<T>) comparator;
    }

}
